const {Adaptador} = require('./Adapters')

const result = Adaptador({
    color: ['verde', 'azul'], 
    info: {name: 'Nelson', edad: 28}
}) //Invocamos al adaptador

console.log(result)