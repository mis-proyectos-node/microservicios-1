
const {Servicio} = require('../Services')

function Adaptador({info, color}) { //Funcion Adaptador

    const result = Servicio({info, color});

    return result;
}

module.exports = { Adaptador }
